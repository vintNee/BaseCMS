<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<html>
<head>
<meta name="viewport"
	content="width=device-width,initial-scale=1,user-scalable=0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页</title>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/aui/css/aui.css"/>'>
<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
</head>
<body>
	<div class="aui-content">
		<ul class="aui-grid-nine">
			<c:forEach items="${catgories }" var="menu">
				<li class="aui-col-xs-4 aui-text-center">
					<p>${menu.name }</p></li>
			</c:forEach>
		</ul>
		<ul class="aui-list-view">
		<c:forEach items="${articles }" var="article">
			<li class="aui-list-view-cell aui-img">
					<!-- <img class="aui-img-object aui-pull-left" src="../image/demo2.png"> -->
			    <a href="article?id=${article.id }">
					<div class="aui-img-body">
						${article.title }
						<p>${article.remark }</p>
					</div>
				</a>
			</li>
		</c:forEach>
		</ul>
	</div>
	<footer class="aui-nav" id="aui-footer">
		<ul class="aui-bar-tab">
			<li class="active-warning" id="tab1" onclick="randomSwitchBtn(this)"><span
				class="aui-iconfont aui-icon-home"></span>
				<p>首页</p></li>
			<li id="tab2" onclick="randomSwitchBtn(this)"><span
				class="aui-iconfont aui-icon-mark"></span>
				<p>最热</p></li>
			<li id="tab3" onclick="randomSwitchBtn(this)"><span
				class="aui-iconfont aui-icon-like"></span>
				<p>最新</p></li>
			<li id="tab4" onclick="randomSwitchBtn(this)"><span
				class="aui-iconfont aui-icon-my"></span>
				<p>最爱</p></li>
		</ul>
	</footer>
	<script type="text/javascript">
		function randomSwitchBtn(obj, index) {
			$("ul li.active-warning").removeClass("active-warning");
			$(obj).addClass("active-warning");

		}
	</script>
</body>
</html>