package com.cms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cms.common.hibernate.BaseDao;
import com.cms.service.BaseService;

@Transactional(rollbackFor = Throwable.class, propagation = Propagation.REQUIRED)
public class BaseServiceImpl implements BaseService {
	@Autowired
	@Qualifier("baseDao")
	protected BaseDao baseDao;

	@Override
	public Object get(Class<?> clazz, Integer id) {
		return baseDao.get(clazz, id);
	}

	/** 增加或更新实体. */
	public void saveOrUpdate(Object entity) {
		baseDao.saveOrUpdate(entity);
	}

	/** 删除指定的实体. */
	public void delete(Object entity) {
		baseDao.delete(entity);
	}

	/** 删除指定的实体. */
	public void deleteById(Class<?> clazz, Integer id) {
		baseDao.delete(baseDao.get(clazz, id));
	}

	@Override
	public Long count(Object entity) {
		return baseDao.countByExample(entity);
	}

	@Override
	public <T> List<T> listByExample(T exampleEntity, Integer pageNo,
			Integer pageSize) {
		return baseDao.findByExample(exampleEntity, (pageNo - 1) * pageSize,
				pageNo * pageSize);
	}

	@Override
	public void updateByX(String hql, Object... objects) {
		baseDao.hqlUpdate(hql, objects);
	}

}
