package com.cms.support;

public class PageDto {
	private Long iTotalRecords;// //实际的行数
	private Long iTotalDisplayRecords;// //显示的行数,这个要和上面写的一样
	private Integer iDisplayStart;// 起始页索引
	private Integer iDisplayLength;// 每页显示行数
	private Object aaData;// 数据

	public Long getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Long getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Long iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Integer getiDisplayStart() {
		return iDisplayStart;
	}

	public void setiDisplayStart(Integer iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}

	public Integer getiDisplayLength() {
		return iDisplayLength;
	}

	public void setiDisplayLength(Integer iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}

	public Object getAaData() {
		return aaData;
	}

	public void setAaData(Object aaData) {
		this.aaData = aaData;
	}

	@Override
	public String toString() {
		return "PageDto [iTotalRecords=" + iTotalRecords
				+ ", iTotalDisplayRecords=" + iTotalDisplayRecords
				+ ", iDisplayStart=" + iDisplayStart + ", iDisplayLength="
				+ iDisplayLength + ", aaData=" + aaData + "]";
	}

}
