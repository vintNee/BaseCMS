package com.cms.support;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(Utils.class);

	/**
	 * HTTP POST 发送数据
	 * 
	 * @param url
	 *            请求地址
	 * @param data
	 *            请求数据
	 * @param cr
	 *            herder参数
	 * @return
	 * @throws Exception
	 */
	public static String sendHttpRequest(String url, String data, Logger logger) {
		// 创建链接
		HttpURLConnection hconn = null;
		OutputStream os = null;
		InputStream is = null;
		try {
			hconn = (HttpURLConnection) new URL(url).openConnection();
			hconn.setRequestMethod("POST"); // 设置为post请求
			hconn.setDoInput(true);
			hconn.setDoOutput(true);
			hconn.setUseCaches(false);
			hconn.setRequestProperty("Content-Type", "application/json");
			hconn.setConnectTimeout(30000); // 30s
			hconn.setReadTimeout(30000); // 30s

			// 发送数据
			os = hconn.getOutputStream();
			byte[] f = data.getBytes("utf-8");
			os.write(f, 0, f.length);
			os.flush();
			os.close();
			os = null;

			// 接收数据
			is = hconn.getInputStream();
			List<Byte> byteList = new ArrayList<Byte>();
			byte[] buf = new byte[1];
			while ((is.read(buf)) > 0) {
				byteList.add(buf[0]);
			}
			is.close();
			is = null;
			hconn.disconnect();
			hconn = null;

			String recStr = "";
			int size = byteList.size();
			if (size > 0) {
				byte[] b = new byte[size];
				for (int i = 0; i < size; i++) {
					b[i] = byteList.get(i);
				}
				recStr = new String(b, "utf-8");
			}
			return recStr;
		} catch (Exception e) {
			LOGGER.error("向[#0]发送http post请求失败,错误信息[#1]" + e);
		} finally {
			if (hconn != null) {
				hconn.disconnect();
			}
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					LOGGER.error("向[#0]发送http post请求失败,错误信息[#1]" + e);
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					LOGGER.error("向[#0]发送http post请求失败,错误信息[#1]" + e);
				}
			}
		}
		return null;
	}
	
	public String getStr(String[]args){
		String str="";
		for(int i=0;i<args.length;i++){	
	                 str+=(String)args[i];
		}	
		return str;
	}
	/**
	 * 银行卡号中间屏蔽（*）
	 * @param start 前面几位要显示
	 * @param end 后面几位要显示
	 * @param bankCard 银行卡号
	 * @return
	 */
	public static String dealBankCard(int start,int end,String bankCard){
		if(StringUtils.isNotBlank(bankCard)){
			if(bankCard.length()>start+end){
				String startString = bankCard.substring(0, start);
				String endString = bankCard.substring(bankCard.length() - end);
				StringBuffer sb = new StringBuffer();
				for(int i = 0;i<bankCard.length()-(start+end) ;i++){
					sb.append("*");
				}
				return startString+sb.toString()+endString;
			}
		}
		return bankCard;
	}
	
}
