<%@ taglib prefix="myfn" uri="http://www.cms.com/CMS/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body>
	<div class="wrapper wrapper-content animated fadeIn">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>常用操作</h5>
					</div>
					<div class="ibox-content">

						<div class="row">
							<div class="col-sm-4">
								<div class="panel panel-default">
									<div class="panel-heading">微信菜单同步</div>
									<div class="panel-body">
										<form class="form-inline form-index" action="#" method="post"
											id="form">
											<div class="form-group">
												<label class="control-label">微信账号</label><select
													id="wxAccount" style="width: 200px;" class="form-control">
													<c:forEach items="${accounts }" var="account">
														<option value="${account.account }">${account.account }</option>
													</c:forEach>
												</select> <input type="button" value="同步" onclick="syncWxMenu();"
													class="btn btn-info">
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="panel panel-primary">
									<div class="panel-heading">测试公众号二维码</div>
									<div class="panel-body">
										<img alt="测试微信号二维码" src="<c:url value="/images/ceshi.jpg"/>" width="200">
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="panel panel-success">
									<div class="panel-heading">成功</div>
									<div class="panel-body">
										<p>通过 .panel-heading 可以很简单地为面板加入一个标题容器。你也可以通过添加设置了
											.panel-title 类的标签，添加一个预定义样式的标题。 为了给链接设置合适的颜色，务必将链接放到带有
											.panel-title 类的标题标签内。</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="panel panel-info">
									<div class="panel-heading">
										<i class="fa fa-info-circle"></i> 信息
									</div>
									<div class="panel-body">
										<p>通过 .panel-heading 可以很简单地为面板加入一个标题容器。你也可以通过添加设置了
											.panel-title 类的标签，添加一个预定义样式的标题。 为了给链接设置合适的颜色，务必将链接放到带有
											.panel-title 类的标题标签内。</p>
									</div>

								</div>
							</div>
							<div class="col-sm-4">
								<div class="panel panel-warning">
									<div class="panel-heading">
										<i class="fa fa-warning"></i> 警告
									</div>
									<div class="panel-body">
										<p>通过 .panel-heading 可以很简单地为面板加入一个标题容器。你也可以通过添加设置了
											.panel-title 类的标签，添加一个预定义样式的标题。 为了给链接设置合适的颜色，务必将链接放到带有
											.panel-title 类的标题标签内。</p>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="panel panel-danger">
									<div class="panel-heading">危险</div>
									<div class="panel-body">
										<p>通过 .panel-heading 可以很简单地为面板加入一个标题容器。你也可以通过添加设置了
											.panel-title 类的标签，添加一个预定义样式的标题。 为了给链接设置合适的颜色，务必将链接放到带有
											.panel-title 类的标题标签内。</p>
									</div>
									<div class="panel-footer">面板Footer</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		function syncWxMenu() {
			$.ajax({
				url : "../wxapi/publishMenu",
				data : {
					"wxAccount" : $("#wxAccount").val()
				},
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						layer.alert('菜单更新成功');
					} else {
						layer.alert('操作失败:' + result.obj);
					}
				},
				error : function() {
					layer.alert('操作失败:系统异常');
				}
			});
		}
	</script>
</body>


<!-- Mirrored from www.zi-han.net/theme/hplus/tabs_panels.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:53 GMT -->
</html>
