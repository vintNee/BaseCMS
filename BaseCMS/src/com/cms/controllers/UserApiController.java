package com.cms.controllers;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cms.dto.UserReqDto;
import com.cms.dto.UserRespDto;
import com.cms.entity.User;
import com.cms.service.UserService;
import com.cms.support.Result;

@Controller
@RequestMapping(value = "api")
public class UserApiController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(UserApiController.class);

	@Autowired
	private UserService userService;

	@ResponseBody
	@RequestMapping("/user/list")
	public Object list(UserReqDto user, Integer pageNum, Integer pageSize) {
		UserRespDto userDto = new UserRespDto();
		try {
			userDto = userService.list(user, pageNum, pageSize);
		} catch (Exception e) {
			LOGGER.error("UserController 查询用户错误," ,e);
		}
		return userDto;
	}

	@ResponseBody
	@RequestMapping(value = "/user/regist")
	public Object doAdd(User user) {
		try {
			if (userService.get(user.getUserName()) != null) {
				return new Result(false, "该用户名已注册");
			}
			user.setStatus(0);
			userService.add(user);
			return new Result(true,user);
		} catch (Exception e) {
			LOGGER.error("用户注册错误",e);
			return new Result(false, "系统异常,请稍后再试");
		}
	}

	@ResponseBody
	@RequestMapping(value = "/user/login")
	public Object login(User user) {
		try {
			if (user == null || StringUtils.isBlank(user.getUserName())
					|| StringUtils.isBlank(user.getPassword())) {
				return new Result(false, "用户名密码不能为空");
			}
			User userDb = userService.get(user.getUserName());
			if (userDb == null) {
				return new Result(false, "用户不存在");
			}
			if (userDb.getPassword().equals(user.getPassword())) {
				return new Result(true, userDb);
			} else {
				return new Result(false, "用户名或密码错误");
			}
		} catch (Exception e) {
			LOGGER.error("用户登录错误",e);
			return new Result(false, "系统异常,请稍后再试");
		}
	}

	@ResponseBody
	@RequestMapping(value = "/user/update")
	public Object modify(User user) {
		try {
			if (StringUtils.isBlank(user.getUserName())) {
				return new Result(false, "用户名不能为空");
			}
			User userDb = userService.get(user.getUserName());
			if (!StringUtils.isBlank(user.getNiceName())) {
				userDb.setNiceName(user.getNiceName());
			}
			if (!StringUtils.isBlank(user.getPassword())) {
				userDb.setPassword(user.getPassword());
			}
			if (!StringUtils.isBlank(user.getFaceUrl())) {
				userDb.setFaceUrl(user.getFaceUrl());
			}
			if (user.getStatus() != null) {
				userDb.setStatus(user.getStatus());
			}
			if (!StringUtils.isBlank(user.getRoadLicense())) {
				userDb.setRoadLicense(user.getRoadLicense());
			}
			if (!StringUtils.isBlank(user.getCarImg())) {
				userDb.setCarImg(user.getCarImg());
			}
			if (user.getCarType()!=null) {
				userDb.setCarType(user.getCarType());
			}
			if (!StringUtils.isBlank(user.getTaxiLicense())) {
				userDb.setTaxiLicense(user.getTaxiLicense());
			}
			if (!StringUtils.isBlank(user.getDrivingLicense())) {
				userDb.setDrivingLicense(user.getDrivingLicense());
			}
			userService.update(userDb);
			return new Result(true, userDb);
		} catch (Exception e) {
			LOGGER.error("UserCardController 修改用户信息错误,",e);
			return new Result(false, "系统异常,请稍后再试");
		}
	}

	// 修改密码
	@ResponseBody
	@RequestMapping(value = "/user/updatePwd")
	public Object modifyPass(String userName, String password, String newPass) {
		User user;
		try {
			user = userService.get(userName);
			if (!password.equals(user.getPassword())) {
				return new Result(false, "原密码不正确");
			}
			if (StringUtils.isBlank(userName)) {
				return new Result(false, "用户名不能为空");
			}
			if (StringUtils.isBlank(password)) {
				return new Result(false, "密码不能为空");
			}
			if (StringUtils.isBlank(newPass)) {
				return new Result(false, "新密码不能为空");
			}
			if (password.equals(newPass)) {
				return new Result(false, "新密码不能与旧密码相同");
			}
			
			user.setPassword(newPass);
			userService.update(user);
			return new Result(true, user);
		} catch (Exception e) {
			LOGGER.error("用户修改密码异常",e);
			return new Result(false, "系统异常,请稍候再试");
		}
	}

	// 用户信息
	@ResponseBody
	@RequestMapping(value = "/user/view")
	public Object view(String userName) {
		User user = new User();
		try {
			user = userService.get(userName);
		} catch (Exception e) {
			LOGGER.error("查看用户信息异常",e);
		}
		return user;
	}
	// 批量查询
	@ResponseBody
	@RequestMapping(value = "/user/getUsers")
	public Object getUsers(String users) {
		List<User> userList = new LinkedList<User>();
		try {
			userList = userService.getUsers(users);
		} catch (Exception e) {
			LOGGER.error("批量查询用户信息异常",e);
		}
		return userList;
	}

	/**
	 * 重置密码
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/user/resetPwd")
	public Object restPwd(String userName) {
		try {
			User user = userService.get(userName);
			MessageDigestPasswordEncoder encoder = new MessageDigestPasswordEncoder(
					"md5");
			String password = encoder.encodePassword("CMS#123", null);
			user.setPassword(password);
			userService.update(user);
		} catch (Exception e) {
			LOGGER.error("密码重置失败" + e.getLocalizedMessage());
			e.printStackTrace();
			return new Result(false, "系统异常,请稍后再试");
		}
		return new Result(true, "密码已重置为CMS#123，请登录修改密码！");
	}
}
