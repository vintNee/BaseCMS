<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body>
	<html:view titles="公众号,公众号名称,appid,密钥,url地址,token,二维码"
		ids="account-input,name-input,appid-input,appsecret-input,url-input,token-input,dubbleMa-img"
		idValues="${entity.account },${entity.name },${entity.appid },${entity.appsecret },${entity.url },${entity.token },${entity.dubbleMa }">
		<%-- <div class="item">
			<span class="preTitle">状态:</span><font color="red"></font> 
			<html:select cssClass="form-control" collection="articleStatus" selectValue="${entity.status }" name="status" id="status">
			</html:select>
		</div> --%>
	</html:view>
</body>
</html>