package com.cms.service;

import com.cms.support.Result;

public interface SmsVerifyCodeService {
	
	/**
	 * 发送短信验证码
	 * @param dto
	 * @return
	 */
	public Result sendSms(String phone,String id);

	/**
	 * 验证短信验证码
	 * @param dto
	 * @return 
	 */
	public Result verifySms(String codeId,String phone,String code);
	
	
	/**
	 * 删除消息创建时间大于24小时的验证信息
	 * @param dto
	 * @return 
	 */
	public void deleteSms();

}
