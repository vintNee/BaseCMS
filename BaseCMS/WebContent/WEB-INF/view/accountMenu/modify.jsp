<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body>
	<html:modify itemId="${entity.id }"
		titles="菜单名称,inputCode,url,排序,父级菜单id,父级菜单名称,微信账号"
		ids="name_required-input,inputCode_required-input,url_none-input,sort_required-input,parentId_none-input,parentName_none-input,wxAccount_none-input"
		idValues="${entity.name },${entity.inputCode },${entity.url },${entity.sort },${entity.parentId },${entity.parentName },${entity.wxAccount }">
		 <div class="item">
			<span class="preTitle">状态:</span><font color="red"></font> 
			<html:select cssClass="form-control" collection="articleStatus"  name="status" id="status" selectValue="${entity.status }">
			</html:select>
		</div>
		 <div class="item">
			<span class="preTitle">事件类型:</span><font color="red"></font> 
			<html:select cssClass="form-control" collection="menuEventType"  name="mtype" id="mtype" selectValue="${entity.mtype }">
			</html:select>
		</div>
	</html:modify>
</body>
</html>