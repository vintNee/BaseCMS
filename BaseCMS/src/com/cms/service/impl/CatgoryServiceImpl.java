package com.cms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cms.entity.Catgory;
import com.cms.service.CatgoryService;

@Service
public class CatgoryServiceImpl extends BaseServiceImpl implements
		CatgoryService {

	@Transactional
	@Override
	public Catgory loadById(Integer id) {
		return baseDao.get(Catgory.class, id);
	}

	@Transactional
	@Override
	public List<Catgory> list(String wxAccount,String name,String status, String desc, Integer from, Integer to) {
		// 查询结果
		List<Object> any = new ArrayList<Object>();
		any.add(Transformers.aliasToBean(Catgory.class));

		if (StringUtils.isNotBlank(name)) {
			any.add(Restrictions.like("name", name.trim(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(desc)) {
			any.add(Restrictions.like("description", desc.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(wxAccount)) {
			any.add(Restrictions.like("wxAccount", wxAccount.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(status)) {
			any.add(Restrictions.eq("status", status.trim()));
		}
		any.add(Projections.property("id").as("id"));
		any.add(Projections.property("name").as("name"));
		any.add(Projections.property("description").as("description"));
		any.add(Projections.property("showOrder").as("showOrder"));
		any.add(Projections.property("status").as("status"));
		any.add(Projections.property("modifyDate").as("modifyDate"));
		any.add(Projections.property("wxAccount").as("wxAccount"));

		List<Catgory> entity = baseDao.findByAny(Catgory.class, from, to,
				any.toArray());
		return entity;
	}

	@Transactional
	@Override
	public Long getCount(String wxAccount,String name,String status, String desc) {
		// 查询结果
		List<Criterion> any = new ArrayList<Criterion>();
		// 查询条件
		if (StringUtils.isNotBlank(name)) {
			any.add(Restrictions.like("name", name.trim(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(desc)) {
			any.add(Restrictions.like("description", desc.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(wxAccount)) {
			any.add(Restrictions.like("wxAccount", wxAccount.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(status)) {
			any.add(Restrictions.eq("status", status.trim()));
		}
		return baseDao.countByAny(Catgory.class,
				any.toArray(new Criterion[any.size()]));
	}

	@Transactional
	@Override
	public Catgory add(Catgory entity) {
		 baseDao.save(entity);
		 return entity;
	}

	@Transactional
	@Override
	public boolean modify(Catgory entity) {
		try {
			baseDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean delete(Integer id) {
		try {
			Catgory entity = new Catgory();
			baseDao.delete(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
