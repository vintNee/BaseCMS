package com.cms.entity.weixin;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.cms.common.hibernate.BaseEntity;

/**
 * 账号菜单
 * 
 */
@SuppressWarnings("serial")
@Entity(name = "t_weixin_menu")
public class AccountMenu extends BaseEntity {

	@Column
	private String mtype;// 消息类型： click - 事件消息；view - 链接消息

	/**
	 * 事件消息类型；即mtype = click; 系统定义了2中模式 key / fix key 即是 inputcode ； fix 即是
	 * 固定消息id，在创建菜单时，用 _fix_开头，方便解析； 同样的开发者可以自行定义其他事件菜单
	 */
	@Column
	private String eventType;
	@Column
	private String name;// 菜单名称
	@Column
	private String inputCode;
	@Column
	private String url;
	@Column
	private Integer sort;// 排序
	@Column
	private Long parentId;// 父级菜单id
	@Column
	private String msgId;// 消息id
	@Column
	private String parentName;// 父级菜单名称
	@Column
	private String wxAccount;// 微信账号
	@Column
	private String status;// 状态

	public String getMtype() {
		return mtype;
	}

	public void setMtype(String mtype) {
		this.mtype = mtype;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getWxAccount() {
		return wxAccount;
	}

	public void setWxAccount(String wxAccount) {
		this.wxAccount = wxAccount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		if (parentId == null) {
			parentId = 0L;
		}
		this.parentId = parentId;
	}

	public String getInputCode() {
		return inputCode;
	}

	public void setInputCode(String inputCode) {
		this.inputCode = inputCode;
	}

	@Override
	public String toString() {
		return "AccountMenu [mtype=" + mtype + ", eventType=" + eventType
				+ ", name=" + name + ", inputCode=" + inputCode + ", url="
				+ url + ", sort=" + sort + ", parentId=" + parentId
				+ ", msgId=" + msgId + ", parentName=" + parentName
				+ ", wxAccount=" + wxAccount + ", status=" + status + "]";
	}

}
