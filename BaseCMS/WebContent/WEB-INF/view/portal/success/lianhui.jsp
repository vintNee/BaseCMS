<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>爱雅安</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/success.css"/>" />
</head>
<body>
	<div class="outDiv">
		<div class="container con-container">	
			<img src="<c:url value="/images/portal/proCase/lianhuiTop.png"/>" class="img"/>
			<div class="content-one"><span style="color:#000;"><b>联汇通宝</b></span>是一家专业从事移动金融支付产品研发与运营的公司并与银联有长期深度的合作，其产品广泛应用于手机终端及便民支付终端，为用户实现安全高效的移动支付。</div>
			<div class="content-one">卡卡付NFC支付助力联汇通宝互联网金融理财。NFC支付通过芯片卡银行卡拍卡完成支付，安全高效，使理财变得轻松便捷。</div>
		</div>
	</div>
</body>
</html>