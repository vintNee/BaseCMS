package com.cms.common.util;

import java.io.File;

/** 文件处理工具. */
public abstract class FileUtil {
	
	/** 取得文件扩展名. */
	public static String getExtension(File f) {
		return f == null ? null : getExtension(f.getName());
	}

	/** 取得文件扩展名. */
	public static String getExtension(String filename) {
		if (filename == null) {
			return null;
		}
		
		int i = filename.lastIndexOf('.');
		if ((i > -1) && (i < (filename.length() - 1))) {
			return filename.substring(i + 1);
		} else {
			return null;
		}
	}

	/** 去掉文件扩展名. */
	public static String trimExtension(String filename) {
		if (filename != null && filename.length() > 0) {
			int i = filename.lastIndexOf('.');
			if ((i > -1) && (i < (filename.length()))) {
				return filename.substring(0, i);
			}
		}
		return filename;
	}
	
}