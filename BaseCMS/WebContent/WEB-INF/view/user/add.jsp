<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form class="form-inline form-index" id="form">
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">用户名:</span> <input
						name="userName" id="userName" type="text" class="form-control" />
				</div>

				<div class="item">
					<font color="white">*</font> <span class="preTitle">昵称:</span> <input
						name="niceName" id="niceName" type="text" class="form-control" />
				</div>
				<div class="item">
					<font color="white">*</font> <span class="preTitle">头像:</span> <input
						type="file" id="faceUrl" class="form-control" name="myfile"
						onChange="ajaxFileUpload('faceUrl')" />
					<div class="upImg"></div>
					<input name="faceUrl" type="hidden">
				</div>
				<input type="button" value="保存" class="btn btn-default" onclick="doSubmit();"> 
			</form>
		</div>
	</div>
	<script type="text/javascript">
		function doSubmit() {
			if($("#userName").val()==""){
				layer.alert("用户名不能为空");
				return;
			}
			var data = $("#form").serialize();
			$.ajax({
				url : "add",
				data : data,
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						layer.alert('操作成功', {
							icon : 6
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); //获取窗口索引
									parent.layer.close(index);
								});
						parent.reload();
					} else {
						layer.alert('操作失败:' + result.obj);
					}
				},
				error : function() {
					layer.alert('操作失败:系统异常');
				}
			});
		}

		function ajaxFileUpload(id) {
			//执行上传文件操作的函数
			$.ajaxFileUpload({
						//处理文件上传操作的服务器端地址(可以传参数,已亲测可用)
						url : '../common/fileUpload',
						secureuri : false, //是否启用安全提交,默认为false 
						fileElementId : id, //文件选择框的id属性
						dataType : 'json', //服务器返回的格式,可以是json或xml等
						success : function(result, status) { //服务器响应成功时的处理函数
							if (result.success) {
								$(":input[name=" + id + "]").val(result.obj);
								$(":input[name=" + id + "]")
										.prev()
										.html(
												"<img  style='vertical-align:middle;' width='200' height='120' src='"
														+ result.obj
														+ "'/> <input type='button' class='btn btn-default' onclick='ajaxFileDelete(this)' value='删除图片'/> <input type='button' class='btn btn-default' onclick='openImg(this)' value='查看原图'/>");
								$("div[id='" + id + "_div']").css("margin-top",
										"78px");
							} else {
								layer.alert('操作失败:' + result.obj);
							}
						},
						error : function(data, status, e) { //服务器响应失败时的处理函数
							layer.alert('系统异常');
						}
					});
		}
		function ajaxFileDelete(node) {
			var imgNode = $(node).prev();
			var nextNode = $(node).next();
			$(node).remove();
			$(imgNode).remove();
			$(nextNode).remove();
		}
		function openImg(that) {
			var imgNode = $(that).prev().prev();
			window.open($(imgNode).attr("src"));
		}
	</script>
</body>
</html>