<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body class="gray-bg">
	<div class="middle-box text-center animated fadeInDown">
		<h1>403</h1>
		<h3 class="font-bold">您没有该功能操作权限,请联系管理员！</h3>

		<div class="error-desc">
			服务器好像出错了... <br />您可以返回主页看看 <br />
			<a href="index-2.html" class="btn btn-primary m-t">主页</a>
		</div>
	</div>
</body>
</html>
