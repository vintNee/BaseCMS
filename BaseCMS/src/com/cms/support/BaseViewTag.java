package com.cms.support;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName: BaseViewTag
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangp
 * @date 2016年5月5日 下午4:28:31
 * 
 */
public class BaseViewTag extends BaseDiyTag {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseViewTag.class);

	// 通过父类TagSupport的属性值pageContext 取得相关的内置对象

	private String titles;
	private String ids;
	private String idValues;

	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		List<String> titlelList = new LinkedList<String>();
		List<String> idList = new LinkedList<String>();
		List<String> idValuseList = new LinkedList<String>();
		if (!StringUtils.isEmpty(titles)) {
			for (String query : titles.split(",")) {
				titlelList.add(query);
			}
		}
		if (!StringUtils.isEmpty(ids)) {
			for (String query : ids.split(",")) {
				idList.add(query);
			}
		}
		if (!StringUtils.isEmpty(idValues)) {
			for (String query : idValues.split(",")) {
				idValuseList.add(query);
			}
		}
		try {
			out.println("<div id=\"main\" style=\"margin-left: auto; margin-right: auto; width: 980\">");
			out.println("<div class=\"table-container\" id=\"content\">");
			out.println("<form class=\"form-inline form-index\" id=\"form\">");
			out.println("<div class=\"ibox float-e-margins\">");
			for (int i = 0; i < idList.size(); i++) {
				String id = idList.get(i);
				out.println("<div  class=\"item\">");
				if (id.endsWith("-input")) {
					id = id.substring(0, id.lastIndexOf("-"));
					out.print("<span class=\"preTitle\">" + titlelList.get(i)
							+ ":</span>");
					out.println("<font color=\"white\"></font>");
					out.println("<input name=\"" + id + "\" id=\"" + id
							+ "\" type=\"text\" value=\"" + idValuseList.get(i)
							+ "\" class=\"form-control\" disabled/>");
				} else if (id.endsWith("-img")) {
					id = id.substring(0, id.lastIndexOf("-"));
					out.print("<span class=\"preTitle\">" + titlelList.get(i)
							+ ":</span>");
					out.println("<font color=\"white\"></font>");
					out.println("<div class=\"upImg\">");
					out.println("<img style='vertical-align: middle;' width='200' height='120' src=\""
							+ idValuseList.get(i) + "\" /> ");
					out.println("<input type='button' class='btn btn-default' onclick='openImg(this)' value='查看原图' /> ");
					out.println("</div>");
				} else if (id.endsWith("-txt")) {
					id = id.substring(0, id.lastIndexOf("-"));
					out.print("<span class=\"preTitle\">" + titlelList.get(i)
							+ ":</span>");
					out.println("<font color=\"white\"></font>");
					out.println("<div class=\"upImg\">");
					out.println("<script type=\"text/plain\" id=\""+id+"\" name=\""+id+"\" disabled style=\"width:1000px;height:240px;\">"+idValuseList.get(i)+"</script>");
					out.println("</div>");
					
					out.println("<script type=\"text/javascript\">");
					out.println("var um = UE.getEditor('"+id+"');");
					out.println("um.setDisabled('fullscreen');");
					out.println("</script>");
					
				}
				out.println("</div>");
			}

		} catch (IOException e) {
			LOGGER.error("listPageTag error-doStartTag:", e);
		}
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public int doEndTag() throws JspException {
		JspWriter out = pageContext.getOut();
		List<String> titlelList = new LinkedList<String>();
		List<String> idList = new LinkedList<String>();
		if (!StringUtils.isEmpty(titles)) {
			for (String query : titles.split(",")) {
				titlelList.add(query);
			}
		}
		if (!StringUtils.isEmpty(ids)) {
			for (String query : ids.split(",")) {
				idList.add(query);
			}
		}
		try {
			out.println("<div class=\"form-group button-group\" style=\"width: 100%;\"> ");
			out.println("<input type=\"button\" value=\"关闭\" class=\"btn btn-default\" onclick=\"closeWindow();\"> ");
			out.println("</div>");
			out.println("</form>");
			out.println("</div>");
			out.println("</div>");
			// js部分
			out.println("<script type=\"text/javascript\">");

			out.println("function closeWindow() {");
			out.println("var index = parent.layer.getFrameIndex(window.name); //获取窗口索引");
			out.println("parent.layer.close(index);");
			out.println("}");

			out.println("function openImg(that) {");
			out.println("var imgNode = $(that).prev();");
			out.println("window.open($(imgNode).attr(\"src\"));");
			out.println("}");

			out.println("</script>");

		} catch (Exception e) {
			LOGGER.error("baseListPage doEnd error:", e);
		}
		return super.doEndTag();
	}

	public String getTitles() {
		return titles;
	}

	public void setTitles(String titles) {
		this.titles = titles;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getIdValues() {
		return idValues;
	}

	public void setIdValues(String idValues) {
		this.idValues = idValues;
	}

}
