package com.cms.controllers.weixin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cms.controllers.BaseController;
import com.cms.entity.weixin.MsgNews;

/**
 * @ClassName: MsgNewsController
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangp
 * @date 2016年5月10日 上午11:42:42
 * 
 */
@Controller
@RequestMapping(value = "msgnews")
public class MsgNewsController extends BaseController<MsgNews> {

	@Override
	protected String getPrefix() {
		return "msgnews";
	}

	@Override
	protected Class<?> getClazz() {
		return MsgNews.class;
	}

}
