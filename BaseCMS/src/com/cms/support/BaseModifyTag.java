package com.cms.support;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName: BaseAddTag
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangp
 * @date 2016年5月5日 下午4:28:31
 * 
 */
public class BaseModifyTag extends BaseDiyTag {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseModifyTag.class);

	// 通过父类TagSupport的属性值pageContext 取得相关的内置对象

	private String titles;
	private String ids;
	private String itemId;
	private String idValues;

	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		List<String> titlelList = new LinkedList<String>();
		List<String> idList = new LinkedList<String>();
		List<String> idValuseList = new LinkedList<String>();
		if (!StringUtils.isEmpty(titles)) {
			for (String query : titles.split(",")) {
				titlelList.add(query);
			}
		}
		if (!StringUtils.isEmpty(ids)) {
			for (String query : ids.split(",")) {
				idList.add(query);
			}
		}
		if (!StringUtils.isEmpty(idValues)) {
			for (String query : idValues.split(",")) {
				idValuseList.add(query);
			}
			while (idValuseList.size()<idList.size()) {//防止最后的值为空的情况下取不到值
				idValuseList.add("");
			}
		}
		try {
			out.println("<div id=\"main\" style=\"margin-left: auto; margin-right: auto; width: 980\">");
			out.println("<div class=\"table-container\" id=\"content\">");
			out.println("<form class=\"form-inline form-index\" id=\"form\">");
			out.println("<div class=\"ibox float-e-margins\">");
			for (int i = 0; i < idList.size(); i++) {
				String id = idList.get(i);
				out.println("<div  class=\"item\">");
				if (id.endsWith("-input")) {
					String disabled = "";
					if (id.contains("_disabled")) {
						disabled = "disabled";
					}
					if (id.contains("_required")) {
						id = id.substring(0, id.indexOf("_"));
						out.print("<span class=\"preTitle\">"
								+ titlelList.get(i) + ":</span>");
						out.println("<font color=\"red\">(必填)</font>");
						out.println("<input name=\"" + id + "\" id=\"" + id
								+ "\" type=\"text\" value=\""
								+ idValuseList.get(i)
								+ "\" class=\"form-control\"  " + disabled
								+ "  />");
					} else {
						id = id.substring(0, id.indexOf("_"));
						out.print("<span class=\"preTitle\">"
								+ titlelList.get(i) + ":</span>");
						out.println("<font color=\"white\">(非必填)</font>");
						out.println("<input name=\"" + id + "\" id=\"" + id
								+ "\" type=\"text\" value=\""
								+ idValuseList.get(i)
								+ "\" class=\"form-control\"  " + disabled
								+ "  />");
					}
				} else if (id.endsWith("-img")) {
					String disabled = "";
					if (id.contains("_disabled")) {
						disabled = "disabled";
					}
					if (id.contains("_required")) {
						id = id.substring(0, id.indexOf("_"));
						out.print("<span class=\"preTitle\">"
								+ titlelList.get(i) + ":</span>");
						out.println("<font color=\"red\">(必填)</font>");
						out.println("<input type=\"file\" id=\"" + id
								+ "\" class=\"form-control\" name=\"myfile\" "
								+ disabled + " onChange=\"ajaxFileUpload('"
								+ id + "')\" />");
						out.println("<div class=\"upImg\">");
						out.println("<img style='vertical-align: middle;' width='200' height='120' src=\""
								+ idValuseList.get(i) + "\" /> ");
						out.println("<input type='button' class='btn btn-default' onclick='openImg(this)' value='查看原图' /> ");
						out.println("</div>");
						out.println("<input name=\"" + id
								+ "\" value=\"" + idValuseList.get(i) + "\"  type=\"hidden\">");
					} else {
						id = id.substring(0, id.indexOf("_"));
						out.print("<span class=\"preTitle\">"
								+ titlelList.get(i) + ":</span>");
						out.println("<font color=\"white\">(非必填)</font>");
						out.println("<input type=\"file\" id=\"" + id
								+ "\" class=\"form-control\" name=\"myfile\" "
								+ disabled + "  onChange=\"ajaxFileUpload('"
								+ id + "')\" />");
						out.println("<div class=\"upImg\">");
						out.println("<img style='vertical-align: middle;' width='200' height='120' src=\""
								+ idValuseList.get(i) + "\" /> ");
						out.println("<input type='button' class='btn btn-default' onclick='openImg(this)' value='查看原图' /> ");
						out.println("</div>");
						out.println("<input name=\"" + id
								+ "\" value=\"" + idValuseList.get(i) + "\" type=\"hidden\">");
					}
				} else if (id.endsWith("-txt")) {
					String tempId = id;
					if (id.contains("_required")) {
						id = id.substring(0, id.indexOf("_"));
						out.print("<span class=\"preTitle\">"
								+ titlelList.get(i) + ":</span>");
						out.println("<font color=\"red\">(必填)</font>");
						out.println("<div class=\"upImg\">");
						out.println("<script type=\"text/plain\" id=\""
								+ id
								+ "\" name=\""
								+ id
								+ "\"  style=\"width:1000px;height:240px;\">"+idValuseList.get(i)+"</script>");
						out.println("</div>");
					} else {
						id = id.substring(0, id.indexOf("_"));
						out.print("<span class=\"preTitle\">"
								+ titlelList.get(i) + ":</span>");
						out.println("<font color=\"white\">(非必填)</font>");
						out.println("<div class=\"upImg\">");
						out.println("<script type=\"text/plain\" id=\""
								+ id
								+ "\" name=\""
								+ id
								+ "\"  style=\"width:1000px;height:240px;\">"+idValuseList.get(i)+"</script>");
						out.println("</div>");
					}

					out.println("<script type=\"text/javascript\">");
					out.println("var um = UE.getEditor('" + id + "');");
					if (tempId.contains("_disabled")) {
						out.println("um.setDisabled('fullscreen');");
					}
					out.println("</script>");
				}
				out.println("<input type=\"hidden\" name=\"id\" value=\""
						+ itemId + "\"> ");
				out.println("</div>");
			}

		} catch (IOException e) {
			LOGGER.error("listPageTag error-doStartTag:", e);
		}
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public int doEndTag() throws JspException {
		JspWriter out = pageContext.getOut();
		List<String> titlelList = new LinkedList<String>();
		List<String> idList = new LinkedList<String>();
		if (!StringUtils.isEmpty(titles)) {
			for (String query : titles.split(",")) {
				titlelList.add(query);
			}
		}
		if (!StringUtils.isEmpty(ids)) {
			for (String query : ids.split(",")) {
				idList.add(query);
			}
		}
		try {
			out.println("<div class=\"form-group button-group\" style=\"width: 100%;\"> ");
			out.println("<input type=\"button\" value=\"保存\" class=\"btn btn-default\" onclick=\"doSubmit();\"> ");
			out.println("<input type=\"button\" value=\"关闭\" class=\"btn btn-default\" onclick=\"closeWindow();\"> ");
			out.println("</div>");
			out.println("</form>");
			out.println("</div>");
			out.println("</div>");
			// js部分
			out.println("<script type=\"text/javascript\">");
			out.println("function doSubmit() {");
			for (int i = 0; i < idList.size(); i++) {
				String id = idList.get(i);
				if (id.contains("_required")) {
					if (id.contains("-txt")) {
						id = id.substring(0, id.indexOf("_"));
						out.println("if (um.getContent() == \"\") {");
						out.println("layer.alert(\"" + titlelList.get(i)
								+ "必填\");");
						out.println("return;");
						out.println("}");
					} else if (id.contains("-img")) {
						id = id.substring(0, id.indexOf("_"));
						out.println("if($(\"#" + id + "\").attr('src')==\"\"){");
						out.println("layer.alert(\"" + titlelList.get(i)
								+ "必填\");");
						out.println("return;");
						out.println("}");
					} else {
						id = id.substring(0, id.indexOf("_"));
						out.println("if($(\"#" + id + "\").val()==\"\"){");
						out.println("layer.alert(\"" + titlelList.get(i)
								+ "必填\");");
						out.println("return;");
						out.println("}");
					}
				}
			}
			out.println("$(\":disabled\").removeAttr(\"disabled\"); ");
			out.println("var data = $(\"#form\").serialize();");
			out.println("$.ajax({");
			out.println("url : \"add\",");
			out.println("data : data,");
			out.println("type : 'post',");
			out.println("contentType : 'application/x-www-form-urlencoded',");
			out.println("encoding : 'UTF-8',");
			out.println("cache : false,");
			out.println("success : function(result) {");
			out.println("if (result.success) {");
			out.println("layer.alert('操作成功', {");
			out.println("icon : 6");
			out.println("},");
			out.println("closeWindow());");
			out.println("} else {");
			out.println("layer.alert('操作失败:' + result.obj);");
			out.println("}");
			out.println("},");
			out.println("error : function() {");
			out.println("layer.alert('操作失败:系统异常');");
			out.println("}");
			out.println("});");
			out.println("}");

			out.println("function closeWindow() {");
			out.println("parent.reload();");
			out.println("var index = parent.layer.getFrameIndex(window.name); //获取窗口索引");
			out.println("parent.layer.close(index);");
			out.println("}");

			out.println("function ajaxFileUpload(id) {");
			out.println("$.ajaxFileUpload({");
			out.println("url : '../common/fileUpload',");
			out.println("secureuri : false,");
			out.println("fileElementId : id, ");
			out.println("dataType : 'json',");
			out.println("success : function(result, status) { ");
			out.println("if (result.success) {");
			out.println("$(\":input[name=\" + id + \"]\").val(result.obj);");
			out.println("$(\":input[name=\" + id + \"]\").prev().html(\"<img  style='vertical-align:middle;' width='200' height='120' src='\" + result.obj + \"'/> <input type='button' class='btn btn-default' onclick='ajaxFileDelete(this)' value='删除图片'/> <input type='button' class='btn btn-default' onclick='openImg(this)' value='查看原图'/>\");");
			out.println("$(\"div[id='\" + id + \"_div']\").css(\"margin-top\", \"78px\");");
			out.println("} else {");
			out.println("layer.alert('操作失败:' + result.obj);");
			out.println("}");
			out.println("},");
			out.println("error : function(data, status, e) {");
			out.println("layer.alert('系统异常');");
			out.println("}");
			out.println("});");
			out.println("}");

			out.println("function ajaxFileDelete(node) {");
			out.println("var imgNode = $(node).prev();");
			out.println("var nextNode = $(node).next();");
			out.println("$(node).remove();");
			out.println("$(imgNode).remove();");
			out.println("$(nextNode).remove();");
			out.println("}");

			out.println("function openImg(that) {");
			out.println("var imgNode = $(that).prev();");
			out.println("window.open($(imgNode).attr(\"src\"));");
			out.println("}");

			out.println("</script>");

		} catch (Exception e) {
			LOGGER.error("baseListPage doEnd error:", e);
		}
		return super.doEndTag();
	}

	public String getTitles() {
		return titles;
	}

	public void setTitles(String titles) {
		this.titles = titles;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getIdValues() {
		return idValues;
	}

	public void setIdValues(String idValues) {
		this.idValues = idValues;
	}

}
