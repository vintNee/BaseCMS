package com.cms.service;

import java.util.List;

import com.cms.entity.Role;

public interface RoleService {
	public List<Role> listRoles();
	public Role getRole(Integer id);
	public boolean saveRole(Role role);
	public boolean removeById(Integer id);
	public List<Role> listDateRoles(String authority, String description ,Integer from, Integer length);
	public void update(Role role);
	public Long getCount(String authority, String description );
	/**
	 * 根据角色名，查询角色
	 * @param authority
	 * @return
	 */
	public Role queryRoleByAuthority(String authority);
}
