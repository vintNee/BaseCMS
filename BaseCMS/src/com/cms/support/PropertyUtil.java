package com.cms.support;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertyUtil {
	private static Properties getInstans(String fileName) throws IOException {
		InputStream in = PropertyUtil.class.getClassLoader()
				.getResourceAsStream(fileName + ".properties");
		Properties props = new Properties();
		props.load(in);
		return props;
	}

	public static String getValue(String fileName, String key, String logFlag) {
		try {
			return getInstans(fileName).getProperty(key);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

}
