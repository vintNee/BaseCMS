package com.cms.common.util;

import java.util.Collection;
import java.util.StringTokenizer;

/** 字符串工具. */
public abstract class StringUtil {

	/** 字符串连接(集合版). */
	public static String join(Collection<?> strs) {
		StringBuilder sb = new StringBuilder();
		for (Object str : strs) {
			sb.append(str);
		}
		return sb.toString();
	}
	
	/** 字符串连接(数组版). */
	public static String join(Object... strs) {
		StringBuilder sb = new StringBuilder();
		for (Object str : strs) {
			sb.append(str);
		}
		return sb.toString();
	}
	
	/**
	 * 将字符串的IP地址转换为长整型.
	 * 算法： 对IP地址前三段递进的乘上1000，然后相加
	 */
	public static long ip2number(String ipString){
		StringTokenizer token = new StringTokenizer(ipString, ".");
		Long ip = Long.parseLong(token.nextToken()) * 1000 * 1000 * 1000
				+ Long.parseLong(token.nextToken()) * 1000 * 1000
				+ Long.parseLong(token.nextToken()) * 1000
				+ Long.parseLong(token.nextToken()); 
		return ip;
	}

}