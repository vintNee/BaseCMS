<%@ taglib prefix="myfn" uri="http://www.cms.com/CMS/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	line-height: 30px;
}

.dd {
	width: 200px;
}
</style>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form id="form">
				<table class="table table-bordered">
					<tr>
						<td>路径:</td>
						<td><input name="url" id="url" class="form-control"
							value="${action.url }"
							onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
					</tr>
					<tr>
						<td>名称:</td>
						<td><input name="title" id="title" class="form-control"
							value="${action.title }" /></td>
					</tr>
					<tr>
						<td>图标:</td>
						<td><input name="icon" id="icon" class="form-control"
							value="${action.icon }" /></td>
					</tr>
					<tr>
						<td>当前所属上级:</td>
						<td style="float: left;"><input name="parentName"
							id="parentName" disabled class="form-control"
							value="${action.parentName }" /></td>
					</tr>
					<tr>
						<td>所属上级:</td>
						<td style="float: left;">
							<div id="actionView"></div>
						</td>
					</tr>
				</table>
				<input type="hidden" id="id" name="id" value="${action.id }" /> <input
					type="hidden" id="parentId" name="parentId"
					value="${action.parentId }" />
				<div style="clear: both"></div>
				<input type="button" value="保存" onclick="doSubmit();"
					class="btn btn-default">
			</form>
		</div>
		<div class="ibox-content">
			<table class="table">
				<thead>
					<tr>
						<th>图标值</th>
						<th>图标</th>
						<th>图标值</th>
						<th>图标</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>fa-home</td>
						<td><i class="fa fa-home"></i></td>
						<td>fa-bar-chart-o</td>
						<td><i class="fa fa-bar-chart-o"></i></td>
					</tr>
					<tr>
						<td>fa-desktop</td>
						<td><i class="fa fa-desktop"></i></td>
						<td>fa-flask</td>
						<td><i class="fa fa-flask"></i></td>
					</tr>
					<tr>
						<td>fa-table</td>
						<td><i class="fa fa-table"></i></td>
						<td>fa-picture-o</td>
						<td><i class="fa fa-picture-o"></i></td>
					</tr>
						<tr>
						<td>fa-magic</td>
						<td><i class="fa fa-magic"></i></td>
						<td>fa-cart-arrow-down</td>
						<td><i class="fa fa-cart-arrow-down"></i></td>
					</tr>
					<tr>
						<td>fa-envelope</td>
						<td><i class="fa fa-envelope"></i></td>
						<td>fa-bell</td>
						<td><i class="fa fa-bell"></i></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$("#actionView").treeview({
				data : '${actionJson}',
				//	data : '[{"text": "根节点","nodes": [{"text": "安全管理","id":"23","nodes": [{"text": "用户管理"},{"text": "角色管理"},{"text": "资源管理"},{"text": "操作日志"},{"text": "数据字典"}]},{"text": "数据操作","nodes": [{"text": "用车管理"},{"text": "会员管理"},{"text": "订单管理"}]}]}]',
				onNodeSelected : function(e, o) {
					$("#parentId").val(o.id);
				}
			})
		})
		function doSubmit() {
			var data = $("#form").serialize();
			$.ajax({
				url : "modify",
				data : data,
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						layer.alert('操作成功', {
							icon : 6
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); //获取窗口索引
									parent.layer.close(index);
								});
						parent.reload();
					} else {
						layer.alert('操作失败:' + result.obj);
					}
				},
				error : function() {
					layer.alert('系统异常');
				}
			});
		}
	</script>
</body>
</html>